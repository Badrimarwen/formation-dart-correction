/*Créer une fonction CheckIfUserValid() qui permet de vérifier un User   
 et UserToken qui seront créer manuellement 
[la variable createdAt (String de format Day/Month/Year) avec la date du jour random et doit étre inférieur ou égale à la date d'aujourd'hui] 
à travers la fonction fetchUserData() (5 secondes de delay) 
si le token est valide ou non (comparaison avec la date d'aujourd'hui). */

// Le Message à afficher pour un token valide Date = Aujourd'hui: Utilisateur $user.username est authentifié

// Le Message à afficher pour un token invalide: Utilisateur $user.username n'a pas un accès

import 'dart:math';
//  tp_06_dart_class_objects.dart

class User {
  String? username;
  String? email;
  UserToken? token;
  User({this.username, this.email, this.token});
}

class UserToken {
  String? token;
  int? expireOn;
  String? createdAt;
  UserToken({this.token, this.expireOn, this.createdAt});
  UserToken.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    expireOn = json['expireOn'];
    createdAt = json['createdAt'];
  }
}

// tp_03_dart_functions.dart
int random(min, max) {
  return min + Random().nextInt(max - min);
}

// tp_05_dart_conditionals_loops.dart
bool isEqualToToday(String date) {
  var today = DateTime.now();
  if (today.day.compareTo(int.parse(date)) == 0) {
    return true;
  }
  return false;
}

// tp_final_functions_async.dart
Future<String> checkIfUserValid() async {
  User user = await fetchUserData();
  // tp_02_dart_variables.dart
  final String date = user.token!.createdAt!.substring(0, 1);
  final String username = user.username!;
  if (isEqualToToday(date)) {
    return 'Utilisateur $username est authentifié';
  } else {
    return "Utilisateur $username n'a pas un accès";
  }
}

Future<User> fetchUserData() {
  var today = DateTime.now();

  User user = User();
  UserToken userToken = UserToken();

  userToken.createdAt = random(1, today.day + 1).toString() +
      "/" +
      today.month.toString() +
      "/" +
      today.year.toString();
  userToken.expireOn = 3600;
  userToken.token = "ghkjksljdlqskjdsdljfqdsf1qs23d1fs231df2s";

  user.token = userToken;
  user.username = "MadMax";
  user.email = "madMax@gmail.com";
  return Future.delayed(
    const Duration(seconds: 5),
    () => user,
  );
}
// Imagine that this function is
// more complex and slow.

Future<void> main() async {
  print('Waiting for user validation...');
  print(await checkIfUserValid());
}
