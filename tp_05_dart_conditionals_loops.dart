import 'dart:math';

void main() {
  final speedLimit = 80;
  final int currentSpeed = Random().nextInt(220) + 1;

  void evaluateSpeed() {
    if (currentSpeed > speedLimit) {
      print('Vous avez dépasser la limite authorisée');
      // TODO afficher un message a l'utilisateur si sa vitesse depasse la limite autorisée
    }
  }
}

// ##################################################################################################

void main2() {
  // TODO : Reprendre l'exemple de userBadges du TP_04
  Map<String, List<String>> userBadges = {
    "user1": ["badge1", "badge2"],
    "user2": ["badge1", "badge4"],
  };

  // TODO : developper une fonction qui cherche les utilisateur qui ont un badge qui commence par le motif passé en parametre

  List<String> retrieveUsersWithBadgeStartWith(String motif) {
    List<String> users = [];
    for (var user in userBadges.keys) {
      for (var badge in userBadges[user]!) {
        if (badge.startsWith(motif)) {
          users.add(user);
        }
      }
    }
    return users;
    // TODO
  }
}
