// TODO : Creer une class User (username, email, UserToken token)
// TODO : Creer une class UserToken (token, expireOn, createdAt)
// TODO : creer un constructeur fromJSON dans pour la class UserToken
class User {
  String? username;
  String? email;
  UserToken? token;
  User({this.username, this.email, this.token});
}

class UserToken {
  String? token;
  int? expireOn;
  String? createdAt;
  UserToken({this.token, this.expireOn, this.createdAt});
  UserToken.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    expireOn = json['expireOn'];
    createdAt = json['createdAt'];
  }
}

void main() {}

// ##################################################################################################
