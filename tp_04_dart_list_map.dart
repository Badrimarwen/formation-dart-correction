void main() {
  // TODO : créer une Map qui contient comme clé le nom des utilisateur et en valeur une list de String qui represente les badges de l'utilisateur

  // initialiser la Map avec des données de test
  Map<String, List<String>> userBadges = {
    "user1": ["badge1", "badge2"],
    "user2": ["badge1", "badge4"],
  };

  // TODO 2 : Creer une fonction qui ajoute un badge à un utilisateur

  void addBadgeToUser(String username, String badgeId) {
    userBadges[username]!.add(badgeId);
    // TODO
  }

  // TODO 3 Créer une fonction qui supprime un badge à un utilisateur

  void removeIfExistsBadgeFromUser(String username, String badgeId) {
    if (userBadges[username]!.contains(badgeId)) {
      userBadges[username]!.remove(badgeId);
    }
  }

  // TODO 4 : Créer une fonction qui affiche les badges d'un utilisateur
  void displayUserBadges(String username) {
    print('Badges de l\'utilisateur $username : ${userBadges[username]!}');
    //TODO
  }

  // TODO 5 : Utiliser les fonctions ci dessus pour ajouter/supprimer/afficher les badges des utilisateurs
}
